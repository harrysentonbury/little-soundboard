# **little-soundboard**

### An eight slot soundboard.

**For punks and cable biters.**

If your tongue is covered in piercings and / or electricity burns,
let this handy little soundboard do all the talking for you.

![screenshot](images/Soundboard_screenshot_1.jpg)

Load in or record .wav and .mp3 samples. Toggle the 'Play Mode' to 'Record Mode'
to use a microphone. Just click on the red buttons or use the keys 1 to 8
(or tab then space) to record microphone.
Click the big green buttons or use keys 1 to 8 in 'Play Mode' to play them back.
Label the buttons with the Labeller.
Save the whole board of files for later. Load them back in or just
record or load some more or do both.

External input and or output devices supported.

***Keyboard Shortcuts***

  | Key        | Does                 |
  | ---------- |:--------------------:|
  | 1 to 8     | Play / Record slots  |
  | s          | Stop  play / record  |
  | t          | Toggle play / record |

## >= python3.6

```
pip3 install numpy
pip3 install scipy
pip3 install sounddevice
pip3 install audio2numpy
```
Also get tkinter.

```
sudo apt install python-tk
```
Get ffmpeg for mp3 support.

```
sudo apt install ffmpeg
```

git clone or download and then extract. cd into little-soundboard.

```
cd little-soundboard
```
Run little-soundboard.

```
python3 little_soundboard.py
```
