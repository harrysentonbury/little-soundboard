#!/usr/bin/env python3

# Copyright (C) 2020 Harry Sentonbury
# GNU General Public License v3.0

from audio2numpy import open_audio
import numpy as np
import scipy.io.wavfile as wf
import sounddevice as sd
import time
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import os
import threading


def timer():
    """limits recording time"""
    n = 0
    while is_play_rec[1] is True:
        time.sleep(0.5)
        n += 0.5
        if n >= duration:
            sd.stop()
            button_normal()
            is_play_rec[1] = False
    else:
        button_normal()


def button_normal():
    """returns buttons to normal enabled state"""
    for i in range(8):
        button_n = eval(f'button_{i}')
        button_n.update()
        button_n.config(state="normal")
    rec_label.config(text="")


def recording(file):
    device = input_device_num.get()
    device = device if device >= 0 else None
    is_play_rec[1] = True
    sound_blox[file] = sd.rec(int(duration * sample_rate), device=device,
                              samplerate=sample_rate, channels=2, dtype='int16')
    timer_thread = threading.Thread(target=timer)
    timer_thread.start()


def stop_rec(event=None):
    sd.stop()
    is_play_rec[1] = False
    global sound
    sound = np.zeros((blocksize, 2))
    if is_play_rec[0] is False:
        toggle()


def play(file):
    global sound
    sound = sound_blox.get(file)


def stream_func(device=-1):
    def callback(outdata, frames, time, status):
        try:
            stream_data = next(sound_slice)
            if stream_flag[0] is False:
                raise sd.CallbackStop
            else:
                outdata[:] = stream_data
        except ValueError:
            outdata[:, :] = np.zeros((blocksize, 2))

    device = device if device >= 0 else None
    stream = sd.OutputStream(device=device, channels=2, dtype="int16", callback=callback,
                             blocksize=blocksize, samplerate=sample_rate)
    with stream:
        while stream_flag[0] is True:
            time.sleep(0.5)
        else:
            stream.__exit__()


def gen():
    """generate sound chunks"""
    global sound
    while True:
        slice = sound[:blocksize, :]
        yield slice
        sound = sound[blocksize:, :]


def do(file):
    """if recording then first disable all other rec buttons"""
    if is_play_rec[0] is True:
        play(file)
    else:
        rec_label.config(text="RECORDING", font='Times 15', fg="red")
        for i in range(8):
            button_n = eval(f'button_{i}')
            button_n.config(state="disabled")
            button_n.update()
        recording(file)


def toggle(event=None):
    """toggle the play mode record mode button"""
    is_play_rec[0] = not is_play_rec[0]
    if is_play_rec[0] is False:
        toggle_button.config(text="Record Mode", bg="#aa0e0e",
                             activebackground="#d6001a")
        for i in range(8):
            dis = f'button_{i}'
            eval(dis).config(bg="#5c0000", fg="#a798b1",
                             activebackground="#8e0213")
    else:
        toggle_button.config(text="Play Mode", bg="#728C00",
                             activebackground="#9fc301")
        if is_play_rec[1] is True:
            stop_rec()
        for i in range(8):
            dis = f'button_{i}'
            eval(dis).config(bg="#21261e", fg="#a798b1",
                             activebackground="#728C00")


def message_win_func(mtitle, blah):
    """a window to display messages"""
    def closer():
        ms_win.destroy()

    global ms_win
    ms_win = tk.Toplevel(master)
    ms_win.title(mtitle)
    label = tk.Label(ms_win, text=blah, font='Times 20', wraplength=500)
    button = tk.Button(ms_win, text='OK', width=6,
                       bg="#728C00", fg="white", command=closer)
    ms_win.bind('<Return>', lambda event=None: button.invoke())

    label.pack(padx=30, pady=10)
    button.pack(pady=20)
    ms_win.lift()


def message_win(mtitle, blah):
    if ms_win is None:
        message_win_func(mtitle, blah)
        return
    try:
        ms_win.lift()
    except tk.TclError:
        message_win_func(mtitle, blah)


def labeler_func():
    """type text on the buttons"""
    def closer():
        labeler_window.destroy()

    def set_text(tag0, tag1):
        tag1['text'] = tag0.get()

    global labeler_window
    labeler_window = tk.Toplevel(master, padx=60, pady=10)
    labeler_window.geometry("500x330")
    labeler_window.title("Labeler")

    entry_label_0 = tk.Label(labeler_window, text="1")
    entry_label_1 = tk.Label(labeler_window, text="2")
    entry_label_2 = tk.Label(labeler_window, text="3")
    entry_label_3 = tk.Label(labeler_window, text="4")
    entry_label_4 = tk.Label(labeler_window, text="5")
    entry_label_5 = tk.Label(labeler_window, text="6")
    entry_label_6 = tk.Label(labeler_window, text="7")
    entry_label_7 = tk.Label(labeler_window, text="8")

    entry_button_0 = tk.Button(
        labeler_window, text="Set Label 1", command=lambda: set_text(entry_0, button_0))
    entry_button_1 = tk.Button(
        labeler_window, text="Set Label 2", command=lambda: set_text(entry_1, button_1))
    entry_button_2 = tk.Button(
        labeler_window, text="Set Label 3", command=lambda: set_text(entry_2, button_2))
    entry_button_3 = tk.Button(
        labeler_window, text="Set Label 4", command=lambda: set_text(entry_3, button_3))
    entry_button_4 = tk.Button(
        labeler_window, text="Set Label 5", command=lambda: set_text(entry_4, button_4))
    entry_button_5 = tk.Button(
        labeler_window, text="Set Label 6", command=lambda: set_text(entry_5, button_5))
    entry_button_6 = tk.Button(
        labeler_window, text="Set Label 7", command=lambda: set_text(entry_6, button_6))
    entry_button_7 = tk.Button(
        labeler_window, text="Set Label 8", command=lambda: set_text(entry_7, button_7))

    entry_0 = tk.Entry(labeler_window)
    entry_1 = tk.Entry(labeler_window)
    entry_2 = tk.Entry(labeler_window)
    entry_3 = tk.Entry(labeler_window)
    entry_4 = tk.Entry(labeler_window)
    entry_5 = tk.Entry(labeler_window)
    entry_6 = tk.Entry(labeler_window)
    entry_7 = tk.Entry(labeler_window)
    close_button = tk.Button(
        labeler_window, text='Close', width=7, command=closer)

    entry_label_0.grid(row=0, column=0)
    entry_label_1.grid(row=1, column=0)
    entry_label_2.grid(row=2, column=0)
    entry_label_3.grid(row=3, column=0)
    entry_label_4.grid(row=4, column=0)
    entry_label_5.grid(row=5, column=0)
    entry_label_6.grid(row=6, column=0)
    entry_label_7.grid(row=7, column=0)
    close_button.grid(row=8, column=1, pady=20)

    entry_0.grid(row=0, column=1)
    entry_1.grid(row=1, column=1)
    entry_2.grid(row=2, column=1)
    entry_3.grid(row=3, column=1)
    entry_4.grid(row=4, column=1)
    entry_5.grid(row=5, column=1)
    entry_6.grid(row=6, column=1)
    entry_7.grid(row=7, column=1)

    entry_button_0.grid(row=0, column=2)
    entry_button_1.grid(row=1, column=2)
    entry_button_2.grid(row=2, column=2)
    entry_button_3.grid(row=3, column=2)
    entry_button_4.grid(row=4, column=2)
    entry_button_5.grid(row=5, column=2)
    entry_button_6.grid(row=6, column=2)
    entry_button_7.grid(row=7, column=2)
    labeler_window.lift()


def labeler():
    if labeler_window is None:
        labeler_func()
        return
    try:
        labeler_window.lift()
    except tk.TclError:
        labeler_func()


def save_all():
    """saves soundboard as .wav files"""
    stamp = sound_name.get()
    if len(stamp) == 0:
        stamp = "Audio-{}".format(str(time.ctime()
                                      [-16:].replace(" ", "-").replace(":", "-")))
    else:
        stamp = "Audio-{}".format(stamp)
    try:
        os.mkdir(stamp)
        for i in range(8):
            if len(sound_blox.get(f'sound_{i}')) == 0:
                continue
            wav_name = eval(f"button_{i}")["text"]
            wf.write(f'{stamp}/{wav_name}.wav', sample_rate, sound_blox.get(f'sound_{i}'))
    except FileExistsError:
        save_entry.delete(0, last='end')
        sound_name.set("")
        message_win("FileExistsError", f'{stamp} Already Exists')


def saver_func():
    """save soundboard dialog"""
    def on_cancel():
        save_entry.delete(0, last='end')
        sound_name.set("")
        saver_window.destroy()

    global saver_window
    saver_window = tk.Toplevel(master)
    saver_window.title("Save.wav")
    saver_window.geometry('500x200')

    instruct_label = tk.Label(
        saver_window, text="Enter a file name and click Save", font='Times 15')
    save_button = tk.Button(saver_window, bg="#728C00", fg="white",
                            text='Save', command=save_all)
    cancel_button = tk.Button(saver_window, text='Close', command=on_cancel)
    save_entry = tk.Entry(saver_window, textvariable=sound_name)
    save_entry.focus()
    saver_window.bind('<Return>', lambda event=None: save_button.invoke())

    instruct_label.grid(column=0, row=0, columnspan=2, padx=20, pady=10)
    save_entry.grid(column=0, row=1, sticky='e')
    save_button.grid(column=1, row=2, pady=20)
    cancel_button.grid(column=2, row=2, pady=20, padx=20)

    saver_window.lift()


def saver():
    if saver_window is None:
        saver_func()
        return
    try:
        saver_window.lift()
    except tk.TclError:
        saver_func()


def dir_loader_func():
    """load in a saved soundboard dir dialog"""
    def close_dir_window():
        dir_window.destroy()

    def load_dir():
        if list_box.curselection():
            item = list_box.get(list_box.curselection())
            wavfile_list = os.listdir(item)
            for i, wav_file in enumerate(wavfile_list):
                try:
                    rate, data = wf.read(f'{this_dir}/{item}/{wav_file}')
                    if len(data.shape) == 1:
                        data = np.array((data, data)).T
                    sound_blox[f'sound_{i}'] = data
                    eval(f'button_{i}')["text"] = wav_file[:-4]
                except ValueError:
                    message_win(
                        "ValueError", "A file is currupted or wrong")
                    return

    if is_play_rec[0] is False:
        toggle()
    global dir_window
    dir_window = tk.Toplevel(master)
    dir_window.title("Reload Board")
    scrollbar = tk.Scrollbar(dir_window)
    list_box = tk.Listbox(
        dir_window, yscrollcommand=scrollbar.set, width=60, height=20)
    button_close = tk.Button(dir_window, text="Close",
                             width=7, command=close_dir_window)
    button_dir = tk.Button(dir_window, text='Load Directory', bg="#728C00",
                           fg="white", command=load_dir)

    list_box.grid(column=0, row=0, columnspan=2)
    button_dir.grid(column=0, row=1, pady=10)
    button_close.grid(column=1, row=1, pady=10)
    scrollbar.grid(column=2, row=0, sticky=tk.N + tk.S)
    scrollbar.config(command=list_box.yview)
    for entry in os.scandir(this_dir):
        if os.path.isdir(entry):
            list_box.insert(tk.END, entry.name)

    dir_window.lift()


def dir_loader():
    loader_window.destroy()
    if dir_window is None:
        dir_loader_func()
        return
    try:
        dir_window.lift()
    except tk.TclError:
        dir_loader_func()


def loader_func():
    """load in .wav files dialog"""
    def file_finder():
        wav_filename = filedialog.askopenfilename(initialdir=this_dir,
        title="Select a File", filetypes=(
            ("Music audio", "*.wav  *.mp3"), ("All audio files", "*.wav  *.mp3")))

        if wav_filename:
            load(wav_filename)
            wav_entry.config(text=wav_filename)
        else:
            return

    def cycler():
        max_slot = 7
        n = 1
        while True:
            yield n
            n += 1
            if n > max_slot:
                n = 0

    def selector():
        key_name = f'sound_{next(slot)}'
        selection.set(key_name)
        a_label.config(text=key_name)

    def load(file):
        def loading(data):
            if len(data.shape) == 1:
                data = np.array((data, data)).T
            sound_blox[selection.get()] = data
            writing = selection.get().replace("sound", "button")
            head, tail = os.path.split(sound_name)
            eval(writing)["text"] = tail[:-4]

        def loading_error():
            message_win("ValueError", "File Currupt Or Wrong")
            wav_entry.config(text="No File Selected")

        sound_name = file
        if sound_name.endswith(".wav"):
            try:
                rate, data = wf.read(sound_name)
                loading(data)
            except ValueError:
                loading_error()
                return
        elif sound_name.endswith(".mp3"):
            try:
                data, rate = open_audio(sound_name)
                data = np.int16(data * 32767)
                loading(data)
            except ValueError:
                loading_error()
                return
        else:
            message_win("nope", "    wrong    ")
            wav_entry.config(text="No File Selected")

    def close_loader():
        loader_window.destroy()

    if is_play_rec[0] is False:
        toggle()
    slot = cycler()
    global loader_window
    loader_window = tk.Toplevel(master, padx=20, pady=20, bg="#21261e")
    loader_window.geometry("500x300")
    loader_window.title("Loader")
    selection = tk.StringVar()
    selection.set('sound_0')

    a_label = tk.Label(loader_window, text=selection.get(),
                       relief=tk.SUNKEN, width=10, font='Times 15')
    select_button = tk.Button(
        loader_window, text="Select Slot to Load", width=20, bg="#728C00",
        activebackground="#9fc301", command=selector)
    wav_entry = tk.Label(
        loader_window, text="   No File\n    Selected   ", wraplength=450, bg="#a1a1a1")
    load_button = tk.Button(loader_window, text="Find Audio",
                            width=10, bg="#728C00", activebackground="#9fc301",
                            command=file_finder)
    dir_load_button = tk.Button(
        loader_window, text="Load Saved\n Directory", bg='#0ba4a4',
        activebackground='#21e4e4', width=15, height=5, command=dir_loader)
    close_loader_button = tk.Button(
        loader_window, text="Close", width=7, command=close_loader)

    select_button.grid(row=0, column=0)
    a_label.grid(row=0, column=1, padx=8)
    wav_entry.grid(row=1, column=0, columnspan=3, pady=20)
    load_button.grid(row=0, column=2, sticky='e')
    dir_load_button.grid(row=2, column=0, pady=20)
    close_loader_button.grid(row=2, column=2, pady=20)

    loader_window.lift()


def loader():
    if loader_window is None:
        loader_func()
        return
    try:
        loader_window.lift()
    except tk.TclError:
        loader_func()


def device_window_func():
    """Input Output device dialog"""

    def driver_set(input_flag=False):
        if ms_win is not None:
            ms_win.destroy()
        if list_box.curselection() == ():
            message_win(
                'Oi!', '''Select a device \n from the list or cancel
                ''')
            return
        else:
            num = list_box.curselection()[0]
            num_name = sd.query_devices()[num].get('name')  # here
            if input_flag is False:
                device_num.set(num)
                message_win(
                    'Output', 'Device number {} ({}) set as output device'.format(num, num_name))
            else:
                input_device_num.set(num)
                message_win(
                    'Input', 'Device number {} ({}) set as input device'.format(num, num_name))

    def reset_default():
        device_num.set(-1)
        input_device_num.set(-1)
        stream_restart()
        if ms_win is not None:
            ms_win.destroy()
        message_win("Default Device", "Device set to default")

    def stream_restart():
        stream_flag[0] = True
        stream_thread = threading.Thread(
            target=stream_func, args=[device_num.get()])
        stream_thread.start()
        device_window.destroy()

    def on_closing_dw():
        if messagebox.askokcancel('Question', 'Do you want to close devices window?'):
            stream_restart()

    stream_flag[0] = False
    global device_window
    device_window = tk.Toplevel(master)
    device_window.title("Input/Output Devices")
    device_window.config(bg='#afb4b5')

    query = repr(sd.query_devices())
    query = query.split('\n')

    frame_0 = tk.Frame(device_window, relief=tk.RAISED, bd=2, bg='#afb4b5')
    label_0 = tk.Label(device_window, text='List of availible devices',
                       bg='#afb4b5', font='Times 20')
    scrollbar = tk.Scrollbar(device_window)
    label_1 = tk.Label(
        frame_0, text='Select device then set as Input or Output', bg='#afb4b5', font='Times 15')
    set_input_button = tk.Button(frame_0, text='Set Input', height=3, width=6, activebackground='#99c728',
                                 bg="#728C00", fg="white", command=lambda: driver_set(True))
    set_output_button = tk.Button(frame_0, text='Set Output', height=3, width=6, activebackground='#99c728',
                                  bg="#728C00", fg="white", command=driver_set)
    reset_button = tk.Button(
        device_window, text='Reset to Default Device', command=reset_default)
    close_button = tk.Button(
        device_window, text='Close', command=stream_restart)
    list_box = tk.Listbox(
        device_window, yscrollcommand=scrollbar.set, width=60, height=25)
    for i in range(len(query)):
        list_box.insert(tk.END, query[i])
    list_box.selection_set(tk.END)

    label_0.grid(row=0, column=0, columnspan=2)
    list_box.grid(row=1, column=0, columnspan=3)
    scrollbar.grid(row=1, column=3, sticky=tk.N + tk.S)

    frame_0.grid(row=2, column=0, rowspan=2, columnspan=2,
                 sticky='w', pady=5, padx=20)
    label_1.grid(row=2, column=0, sticky='ne', pady=8, padx=5)
    set_input_button.grid(row=3, column=0, pady=5, sticky='e')
    set_output_button.grid(row=3, column=1, pady=5, padx=5)
    close_button.grid(row=3, column=2, sticky='w')
    reset_button.grid(row=4, column=1, sticky='w', pady=8)
    scrollbar.config(command=list_box.yview)

    device_window.protocol('WM_DELETE_WINDOW', on_closing_dw)
    device_window.lift()


def device_select():
    if device_window is None:
        device_window_func()
    try:
        device_window.lift()
    except tk.TclError:
        device_window_func()


def closing_master():
    if messagebox.askokcancel("Quit", "Do you want to Quit, There may be unsaved stuff"):
        close_soundboard()

def close_soundboard():
    master.destroy()
    stream_flag[0] = False


try:
    is_play_rec = [False, False]
    stream_flag = [True]
    blocksize = 512
    sample_rate = 44100
    duration = 40
    this_dir = os.getcwd()
    sound = np.zeros((blocksize, 2))
    sound_slice = gen()
    sound_blox = {'sound_0': sound, 'sound_1': sound, 'sound_2': sound,
                  'sound_3': sound, 'sound_4': sound, 'sound_5': sound,
                  'sound_6': sound, 'sound_7': sound}
    stream_thread = threading.Thread(target=stream_func)
    stream_thread.start()

    master = tk.Tk()
    master.title('Little Soundboard')

    labeler_window = None
    saver_window = None
    loader_window = None
    dir_window = None
    ms_win = None
    device_window = None
    menu_bar = tk.Menu(master)
    menu_bar.add_command(label="Labeler", command=labeler)
    menu_bar.add_command(label="Save", command=saver)
    menu_bar.add_command(label="Loader", command=loader)
    menu_bar.add_command(label="Devices", command=device_select)
    menu_bar.add_command(label="Close", command=close_soundboard)
    menu_bar.config(bg="#21261e", fg="#a798b1", activebackground="#728C00")
    master.config(menu=menu_bar, bg="#21261e")
    sound_name = tk.StringVar()
    device_num = tk.IntVar()
    device_num.set(-1)
    input_device_num = tk.IntVar()
    input_device_num.set(-1)

    button_0 = tk.Button(master, text="Sound 1", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_0'))
    button_1 = tk.Button(master, text="Sound 2", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_1'))
    button_2 = tk.Button(master, text="Sound 3", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_2'))
    button_3 = tk.Button(master, text="Sound 4", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_3'))
    button_4 = tk.Button(master, text="Sound 5", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_4'))
    button_5 = tk.Button(master, text="Sound 6", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_5'))
    button_6 = tk.Button(master, text="Sound 7", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_6'))
    button_7 = tk.Button(master, text="Sound 8", width=10, wraplength=100,
                         height=5, bg="#5c0000", fg="#a798b1", activebackground="#8e0213",
                         command=lambda: do('sound_7'))
    master.bind("1", lambda z: do("sound_0"))
    master.bind("2", lambda z: do("sound_1"))
    master.bind("3", lambda z: do("sound_2"))
    master.bind("4", lambda z: do("sound_3"))
    master.bind("5", lambda z: do("sound_4"))
    master.bind("6", lambda z: do("sound_5"))
    master.bind("7", lambda z: do("sound_6"))
    master.bind("8", lambda z: do("sound_7"))

    toggle_button = tk.Button(master, text="Record Mode",
                              bg="#aa0e0e", width=8, activebackground="#d6001a",
                              command=toggle)
    master.bind("<t>", toggle)
    stop_button = tk.Button(master, text="stop", bg="#728C00",
                            activebackground="#9fc301", width=8, command=stop_rec)
    master.bind("<s>", stop_rec)
    rec_label = tk.Label(master, bg="#21261e", fg="#a798b1", text="")

    button_0.grid(row=0, column=0)
    button_1.grid(row=0, column=1)
    button_2.grid(row=0, column=2)
    button_3.grid(row=0, column=3)
    button_4.grid(row=1, column=0)
    button_5.grid(row=1, column=1)
    button_6.grid(row=1, column=2)
    button_7.grid(row=1, column=3)
    toggle_button.grid(row=2, column=0, pady=15)
    stop_button.grid(row=2, column=1, pady=15)
    rec_label.grid(row=2, column=2, columnspan=2, pady=15)

    master.protocol("WM_DELETE_WINDOW", closing_master)
    master.mainloop()
except KeyboardInterrupt:
    stream_flag[0] = False
    print("\ninterupted by user")
except Exception as e:
    stream_flag[0] = False
    print(f"{type(e).__name__} : {str(e)}")
